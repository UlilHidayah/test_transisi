<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'transisi_test',
            'email' => 'admin@transisi.id',
            'password' => 'transisi',
        ]);
    }
}
