<html>
<head>
    <title>Add Company</title></head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<body>
    @if(session()->has('alert'))
    @if(session()->get('alert') == 'success')
    <div class="alert alert-success">
        <strong>Success!</strong> Successfully!
    </div>
    @else
    <div class="alert alert-danger">
        <strong>Error!</strong> Gagal insert data employees!
    </div>
    @endif
    @endif
    <a href="{{ route('employees.index') }}">Go to list data</a>
    <br/><br/>

    <form action="{{ route('employees.store') }}" method="POST" enctype="multipart/form-data" name="form1">
        @csrf
        <table width="50%" border="0">
            <tr> 
                <td>Nama</td>
                <td><input type="text" name="nama"></td>
            </tr>
            <tr> 
                <td>Company</td>
                <td>
                    <select name="company">
                        <option value="" selected disabled>--Choose Company--</option>
                        <?php 
                            foreach($companies as $c){
                        ?>
                        <option value="{{ $c->id }}">{{ $c->nama }}</option>
                        <?php 
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr> 
                <td>Email</td>
                <td><input type="text" name="email"></td>
            </tr>
            <tr> 
                <td></td>
                <td><input type="submit" name="Submit" value="Add"></td>
            </tr>
        </table>
    </form>
</body>
</html>
<script> 
</script> 