<html>
<head>
    <title>Add Company</title></head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<body>
    @if(session()->has('alert'))
    @if(session()->get('alert') == 'success')
    <div class="alert alert-success">
        <strong>Success!</strong> Successfully!
    </div>
    @else
    <div class="alert alert-danger">
        <strong>Error!</strong> Gagal update data employee!
    </div>
    @endif
    @endif
    <a href="{{ route('employees.index') }}">Go to list data</a>
    <br/><br/>

    <form action="{{ route('employees.update', $employee->id) }}" method="POST" enctype="multipart/form-data" name="form1">
        @csrf
        @method('PUT')
        <table width="50%" border="0">
            <tr> 
                <td>Nama</td>
                <td><input type="text" name="nama" value="{{ $employee->nama }}"></td>
            </tr>
            <tr> 
                <td>Website</td>
                <td>
                    <select name="company">
                        <option value="" selected disabled>--Choose Company--</option>
                        <?php 
                            $selected = '';
                            foreach($companies as $c){
                                if($c->id == $employee->id){
                                    $selected = 'selected';
                                }
                        ?>
                        <option value="{{ $c->id }}" {{$selected}}>{{ $c->nama }}</option>
                        <?php 
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr> 
                <td>Email</td>
                <td><input type="text" name="email" value="{{ $employee->email }}"></td>
            </tr>
            <tr> 
                <td></td>
                <td><input type="submit" name="Submit" value="Update"></td>
            </tr>
        </table>
    </form>
</body>
</html>

<script> 
</script> 