<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Employees</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            table {
                width:100%;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 15px;
                text-align: left;
                color: black;
            }
            #t01 tr:nth-child(even) {
                background-color: #eee;
            }
            #t01 tr:nth-child(odd) {
                background-color: #fff;
            }
            #t01 th {
                background-color: grey;
                color: white;
            }
        </style>
    </head>
    <body>
    <div class="container">
    @if(session()->has('alert'))
        @if(session()->get('alert') == 'success')
        <div class="alert alert-success">
            <strong>Success!</strong> Success delete!
        </div>
        @else
        <div class="alert alert-danger">
            <strong>Error!</strong> Gagal delete!
        </div>
        @endif
        @endif
        <h3>Data Employees</h3>
        <a href="{{ route('home') }}">Go To Dashboard</a><br/><br/>

        <a href="{{ route('employees.create') }}">Add New</a><br/><br/>
        <table style="width:100%;" id="t01">
            <tr>
                <th>Nama</th>
                <th>Company</th> 
                <th>Email</th>
                <th>Aksi</th>
            </tr>
            
            <?php foreach ($employees as $employee): ?>
                <tr>
                    <td><?php echo $employee->nama; ?></td>
                    <td><?php echo $employee->companies ? $employee->companies->nama : ''; ?></td>
                    <td><?php echo $employee->email; ?></td>
                    <td>
                        <a href="{{route('employees.show', $employee->id)}}" class="btn btn-info m-1">Edit</a>
                        <form action="{{ route('employees.destroy', $employee->id) }}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button class="btn btn-danger m-1">Delete</button>
                        </form>
                    </td>
                    <!-- <td><a href="{{ route('employees.show', $employee->id) }}">Edit</a> | <a href="{{ route('employees.destroy', $employee->id) }}">Delete</a></td> -->
                </tr>
            <?php endforeach; ?>
        </table>
        </div>

        <?php echo $employees->render(); ?>
    </body>
</html>
