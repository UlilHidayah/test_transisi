<!DOCTYPE html>
<html>
<body>

<h1>Bagian 1 : PHP Dasar</h1>
<?php
    $nilai = array(72, 65, 73, 78, 75, 74, 90, 81, 87, 65, 55, 69, 72, 78, 79, 91, 100, 40, 67, 77, 86);
    $average = array_sum($nilai) / count($nilai);
?>
<p>1. $nilai = “72 65 73 78 75 74 90 81 87 65 55 69 72 78 79 91 100 40 67 77 86”;</p>
<ul>
    <li>(1) nilai rata-rata : {{$average}}</li>
    <li>(2) 7 nilai tertinggi : 
        <?php
            rsort($nilai);
            for($i = 0; $i < 7; $i++){
                echo $nilai[$i] . " ";
            }
        ?>
    </li>
    <li>(3) 7 nilai terendah : 
    <?php
        sort($nilai);
        for($i = 0; $i < 7; $i++){
            echo $nilai[$i] . " ";
        }
    ?>
    </li>
</ul>
<p>2. Cek huruf kecil. misal parameter "tranSISI"</p>
<ul>
    <li>Jumlah huruf kecil : 
        <?php
            function cekhurufkecil($string){
                $result = 0;
                for($i = 0; $i < strlen($string); $i++){
                    if(ctype_lower($string[$i])){
                        $result += 1;
                    }
                }
                return $result;
            }
            echo cekhurufkecil("tranSISI");
        ?>
    </li>
</ul>
<p>3. Unigram, bigram, trigram dari sebuah string. misal “Jakarta adalah ibukota negara Republik Indonesia”</p>
<ul>
    <li>
        <?php
            function generateUBT($input)
            {
                $arr_input = explode(' ', $input);
        
                // unigram
                $unigram = '';
                foreach ($arr_input as $item) {
                    $unigram .= $item.', ';
                }
                $unigram = substr($unigram, 0, -2);
        
                // bigram
                $x = 0;
                $bigram = '';
                foreach ($arr_input as $item) {
                    if ($x < 1) {
                        $bigram .= $item.' ';
                        $x++;
                    } else {
                        $bigram .= $item.', ';
                        $x = 0;
                    }
                }
                $bigram = substr($bigram, 0, -2);
        
                // trigram
                $y = 0;
                $trigram = '';
                foreach ($arr_input as $item) {
                    if ($y < 2) {
                        $trigram .= $item.' ';
                        $y++;
                    } else {
                        $trigram .= $item.', ';
                        $y = 0;
                    }
                }
                $trigram = substr($trigram, 0, -2);
        
        
                $result = 'Unigram : '. $unigram . '<br>';
                $result .= 'Bigram : '. $bigram . '<br>';
                $result .= 'Trigram : '. $trigram;
        
                return $result;
            }
        
            echo generateUBT('Jakarta adalah ibukota negara Republik Indonesia');
        ?>
    </li>
</ul>
<p>5.  Fungsi “enkripsi”, yang apabila diberikan input DFHKNQ akan memberikan output EDKGSK”</p>
<ul>
    <li>
        <?php
            function enkripsi($input) 
            {
                $output = '';
                for($i = 0; $i < strlen($input); $i++) 
                {
                    $tmpshift = $i + 1;
                    if($tmpshift % 2 == 0){
                        $tmpshift = $tmpshift * -1;
                    }
                    $temp = ord($input[$i]);
                    $temp = $temp + ($tmpshift);
                    $temp = chr($temp);
                    $output = $output . $temp;
                }
        
                return $output;
            }
        
            echo enkripsi('DFHKNQ');
        ?>
    </li>
</ul>
<p>6. Fungsi dalam PHP untuk melakukan pencarian kata dalam array</p>
<p>$arr = [
 ['f', 'g', 'h', 'i'],
 ['j', 'k', 'p', 'q'],
 ['r', 's', 't', 'u']
];</p>
<?php
    function cari($arr, $input) 
    {
        $output = '';
        $panjang = strlen($input);
        $cek = 0;
        for($i = 0; $i < strlen($input); $i++) 
        {
            for($n = 0; $n < count($arr); $n++) 
            {
                if(in_array($input[$i], $arr[$n])){
                    $cek += 1;
                    continue;
                }
            }
        }

        if($panjang == $cek){
            return 'true';
        }
        return 'false';
    }

    $arr = array(
        ['f', 'g', 'h', 'i'],
        ['j', 'k', 'p', 'q'],
        ['r', 's', 't', 'u']
    );
?>
<ul>
    <li>
        <?php
            echo "misal cari(arr, 'fghi') => ".cari($arr, 'fghi');
        ?>
    </li>
    <li>
        <?php
            echo "misal cari(arr, 'fghp') => ".cari($arr, 'fghp');
        ?>
    </li>
    <li>
        <?php
            echo "misal cari(arr, 'fjrstp') => ".cari($arr, 'fjrstp');
        ?>
    </li>
    <li>
        <?php
            echo "misal cari(arr, 'abrstp') => ".cari($arr, 'abrstp');
        ?>
    </li>
</ul>
</body>
</html>