<html>
<head>
    <title>Add Company</title></head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<body>
    @if(session()->has('alert'))
    @if(session()->get('alert') == 'success')
    <div class="alert alert-success">
        <strong>Success!</strong> Successfully!
    </div>
    @else
    <div class="alert alert-danger">
        <strong>Error!</strong> Gagal update data company!
    </div>
    @endif
    @endif
    <a href="{{ route('companies.index') }}">Go to list data</a>
    <br/><br/>

    <form action="{{ route('companies.update', $company->id) }}" method="POST" enctype="multipart/form-data" name="form1">
        @csrf
        @method('PUT')
        <table width="50%" border="0">
            <tr> 
                <td>Nama</td>
                <td><input type="text" name="nama" value="{{ $company->nama }}"></td>
            </tr>
            <tr> 
                <td>Email</td>
                <td><input type="text" name="email" value="{{ $company->email }}"></td>
            </tr>
            <tr> 
                <td>Logo</td>
                <td>
                    <br>
                    <input type="file" id="logo" name="logo" accept="image/png" value=""  style="display:none">
                    <input type="hidden" id="filename" name="filename" accept="image/png" value="{{ $company->logo }}">
                    <a href="#" class="btn btn-primary" onclick="browse()">Browse</a>
                    <p id="filenametmp">{{ $company->logo }}</p>
                    <p id="size"></p>
                </td>
            </tr>
            <tr> 
                <td>Website</td>
                <td><input type="text" name="website" value="{{ $company->website }}"></td>
            </tr>
            <tr> 
                <td></td>
                <td><input type="submit" name="Submit" value="Update"></td>
            </tr>
        </table>
    </form>
</body>
</html>

<script> 
    var _URL = window.URL || window.webkitURL;
    $("#logo").change(function (e) {
        const fi = document.getElementById('logo'); 
        // Check if any file is selected. 
        if (fi.files.length > 0) { 
            for (let i = 0; i <= fi.files.length - 1; i++) { 
  
                const fsize = fi.files.item(i).size; 
                const file = Math.round((fsize / 1024)); 
                // The size of the file. 
                if (file > 2048) { 
                    alert("File too Big, please select a file less than 2mb"); 
                    fi.value = '';
                    document.getElementById('filenametmp').innerHTML = '<b></b>'; 
                    $('#filename').val(''); 
                    return;
                } 
            } 
            var file, img;
            if ((file = this.files[0])) {
                img = new Image();
                var objectUrl = _URL.createObjectURL(file);
                img.onload = function () {
                    if (this.width < 100 || this.height < 100) { 
                        alert("File too Small : "+this.width+"x"+this.height+"px, please select a file minimum 100x100 px"); 
                        fi.value = '';
                        document.getElementById('filenametmp').innerHTML = '<b></b>'; 
                        $('#filename').val(''); 
                        return;
                    } 
                    $filename = $('#logo').val();
                    $filename = $filename.split("\\")[2];
                    document.getElementById('filenametmp').innerHTML = $filename;
                    $('#filename').val($filename); 
                    _URL.revokeObjectURL(objectUrl);
                };
                img.src = objectUrl;
            }
        } else {
            document.getElementById('filenametmp').innerHTML = '<b></b>'; 
            $('#filename').val(''); 
        }
    });
    function browse(){
        $('#logo').trigger('click');
    }
</script> 