<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Auth;

use App\Employees;
use App\Companies;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            $employees = Employees::with('companies')->paginate(5);
            return view('employees.index', ['employees' => $employees]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Companies::get();
        return view('employees.create', ['companies' => $companies]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nama'       => 'required',
            'email'      => 'required|email',
            'company' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return back()->with('alert', 'failed');
        } else {
            $nama = $request->nama;
            $email = $request->email;
            $company = $request->company;
    
            // Insert Companies data into table
            $result = Employees::insert([
                'nama' => $nama,
                'company' => $company,
                'email' => $email
            ]);

            return back()->with('alert', 'success');
        }
        return back()->with('alert', 'failed');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $companies = Companies::get();
        $employee = Employees::where('id', $id)->first();
        return view('employees.show', ['employee' => $employee, 'companies' => $companies]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'nama'       => 'required',
            'company' => 'required',
            'email'      => 'required|email',
        ];
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return back()->with('alert', 'failed');
        } else {
            // store
            $company = Employees::find($id);
            $company->nama       = $request->nama;
            $company->company = $request->company;
            $company->email      = $request->email;
            $company->save();

            // redirect
            return back()->with('alert', 'success');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Employees::where('id', $id)->delete();
        return back()->with('alert', 'success');
    }
}
