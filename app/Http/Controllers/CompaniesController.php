<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input; 
use Illuminate\Support\Facades\Auth;

use App\Companies;
use App\Employees;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            $companies = Companies::paginate(5);
            return view('companies.index', ['companies' => $companies]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nama'       => 'required',
            'email'      => 'required|email',
            'logo'      => 'required|mimes:png',
            'website' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return back()->with('alert', 'failed');
        } else {
            $nama = $request->nama;
            $email = $request->email;
            $logo = $request->file('logo');
            $logoname = $logo->getClientOriginalName();
            $website = $request->website;
    
            // Insert Companies data into table
            $result = Companies::insert([
                'nama' => $nama,
                'email' => $email,
                'logo' => $logoname,
                'website' => $website,
            ]);
            $logo->move(storage_path('app/company/'),$logoname);

            return back()->with('alert', 'success');
        }
        return back()->with('alert', 'failed');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Companies::where('id', $id)->first();
        return view('companies.show', ['company' => $company]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'nama'       => 'required',
            'email'      => 'required|email',
            // 'logo'      => 'required|mimes:png',
            'filename' => 'required',
            'website' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return back()->with('alert', 'failed');
        } else {
            // store
            $logoname = '';
            if($request->file('logo')){
                $logo = $request->file('logo');
                $logoname = $logo->getClientOriginalName();
                $logo->move(storage_path('app/company/'),$logoname);
            }
            $company = Companies::find($id);
            $company->nama       = $request->nama;
            $company->email      = $request->email;
            $company->logo      = $logoname ? $logoname : $request->filename;
            $company->website = $request->website;
            $company->save();

            // redirect
            return back()->with('alert', 'success');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $any = Employees::where('company', $id)->first();
        if($any){
            return back()->with('alert', 'failed');
        }
        $company = Companies::where('id', $id)->delete();
        return back()->with('alert', 'success');
    }
}
